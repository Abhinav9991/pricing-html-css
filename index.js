const toggleBTN=document.getElementById("toggle")
const basicPlan=document.getElementById("basicPlan")
const professionalPlan=document.getElementById("professionalPlan")
const masterPlan=document.getElementById("masterPlan")


toggleBTN.addEventListener("click",()=>{
    if(toggleBTN.checked){
        basicPlan.textContent = '$19.99'
        professionalPlan.textContent = '$24.99'
        masterPlan.textContent = '$39.99'

    }

    else {

        basicPlan.textContent = '$199.99'
        professionalPlan.textContent = '$249.99'
        masterPlan.textContent = '$399.99'
    }
})